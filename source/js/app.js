// import {Glide} from '../../node_modules/@glidejs/glide/dist/glide.min.js';

let configuration = {
    type: 'carousel',
    startAt: 0,
    perView: 3,
    breakpoints: {
        800: {
            perView: 2
        }
    },
    autoplay: 2500 
};
let glide = new Glide('.glide', configuration);
glide.mount();